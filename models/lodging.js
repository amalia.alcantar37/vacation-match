var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var lodgingSchema = new Schema ({
  type: {type: String, required:true},
  address: {type: String, required: true},
  city: {type: String, required: true},
  state: {type: String, required: true},
  zipcode: {type: String, required: true},
  maxCap: {type: Number, required: true},
  numRooms: {type: Number, required: true},
  numBathrooms: {type: Number, required: true},
  description:{type:String},
  price:{type:Number},
  attachments: {type: String},
  status: {type:String, required: true},
  host:{type:String}
}, { collection : 'lodging' });

module.exports = mongoose.model('Lodging', lodgingSchema);
