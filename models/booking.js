var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var bookingSchema = new Schema ({
  lodgingId:{type:String, required: true},
  address:{type:String},
  host:{type:String, required: true},
  guests:{type:String, required:true},
  check_in_date:{type:Date, required: true},
  check_out_date:{type:Date, required: true},
  check_in_time:{type:String, required: true},
  check_out_time:{type:String, require: true},
  user:{type:String},
  price:{type:Number},
  status:{type:String, required:true},
  user_status:{type:String},
  admin_status:{type:String},
}, { collection : 'booking' });


module.exports = mongoose.model('Booking', bookingSchema);
