var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var flash = require('connect-flash');
var multer = require('multer');
var mongoose = require('mongoose');
const MongoClient = require("mongodb").MongoClient;
var MongoStore = require('connect-mongo')(session);
var ObjectId = require("mongodb").ObjectID;

var CONNECTION_URL = "mongodb+srv://aalcanta:Seulax15@cluster0-djpuv.gcp.mongodb.net/test?retryWrites=true";
var DATABASE_NAME = "dbvacationmatch";



require("./models/user");
require("./models/lodging");
require("./models/booking");
var index = require('./routes/index');
var user = require('./routes/user');
var lodging = require('./routes/lodging');
var login = require('./routes/login');
var booking = require('./routes/booking');
var router = express.Router();
var app = express();


var database, collection;
require("./db");

app.set('views', path.join(__dirname, 'views'));
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));

//set up app
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false })); //to handle body requests
app.use(cookieParser());
app.use('/uploads',express.static('uploads'));
app.use(logger('dev'));
app.use('/routes', router);
//use sessions for tracking logins
app.use(session({secret: 'ssshhhhh', resave: false, saveUnitialized: true}));
app.use(express.static(path.join(__dirname, 'public')));
app.use('/', index);
app.use('/user', user);
app.use('/lodging', lodging);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
