var id = "";
$(function(){

    $("#login").on('click', function(event){
        event.preventDefault();
        var email      = $("#emailLogin").val();
        var password   = $("#passwordLogin").val();

        if( !email || !password){

            $("#msgDiv").addClass('alert-danger').show().html("All fields are required.");

        }
        else{

            $.ajax({
                url: "/login",
                method: "POST",
                data: { email: email, password: password }

            }).done(function(data) {

              var status = (data.status == 'error');
              var message = data.message;
              if(status){

                $("#msgDiv").addClass('alert-danger').show().html(message);
              }
              else{
                $("#msgDiv").removeClass('alert-danger').addClass('alert-success').show().html("You are logged in!");
                window.location = ('/viewAllListings');
              }
            });
        }
    });
});
$(function(){

    $("#logout-button").on('click', function(event){
        event.preventDefault();
            $.ajax({
                url: "/logout",
                method: "GET",

            }).done(function(data) {

                window.location = ('/');
            });
        });
    });
// function for registration form
$(function(){

    $("#register").on('click', function(event){
        event.preventDefault();

        var fullname   = $("#fullname").val();
        var email      = $("#email").val();
        var password   = $("#password").val();
        var cpassword  = $("#cpassword").val();
        var dob        = $("#dob").val();
        var terms      = $('input[name="terms"]:checked').val();

        if(!fullname || !email || !password || !cpassword || !dob){

            $("#msgDiv2").show().html("All fields are required.");

        } else if(cpassword != password){

            $("#msgDiv2").show().html("Passwords should match.");

        } else if (!terms){

            $("#msgDiv2").show().html("Please agree to terms and conditions.");
        }
        else{

            $.ajax({
                url: "/register",
                method: "POST",
                data: { full_name: fullname, email: email, password: password, cpassword: cpassword, dob: dob, terms: terms }

            }).done(function( data ) {

                if ( data ) {
                    if(data.status == 'error'){

                        var errors = '<ul>';
                        $.each( data.message, function( key, value ) {
                            errors = errors +'<li>'+value.msg+'</li>';
                        });

                        errors = errors+ '</ul>';
                        $("#msgDiv2").html(errors).show();
                    }else{
                        $("#msgDiv2").removeClass('alert-danger').addClass('alert-success').html(data.message).show();
                          var modal = document.getElementById('register-test');
                          var login = document.getElementById('login-test');
                          modal.style.display = "none";
                          login.style.display = "block";
                          $("#msgDiv").removeClass('alert-danger').addClass('alert-success').html("Success! Sign in.").show();


                    }
                }
            });
        }
    });
});

$(function(){
  // Get the modal
  var modal = document.getElementById("login-test");

  // Get the button that opens the modal
  var btn = document.getElementById("login-button");

  // Get the <span> element that closes the modal
  //var span = document.getElementsByClassName("close")[0];

  // When the user clicks on the button, open the modal
  $(btn).on('click', function(event){
      event.preventDefault();
      modal.style.display = "block";
    });

  // When the user clicks on <span> (x), close the modal
  // span.onclick = function() {
  //   modal.style.display = "none";
  // }

  // When the user clicks anywhere outside of the modal, close it
  window.addEventListener('click', function(event)
  {
    if (event.target == modal)
    {
      modal.style.display = "none";
      $("#msgDiv").removeClass('alert-danger').addClass('alert-success').html("");
    }
  });
});

$(function(){
  // Get the modal
  var modal = document.getElementById('register-test');

  // Get the button that opens the modal
  var btn = document.getElementById("register-button");

  // Get the <span> element that closes the modal
  //var span = document.getElementsByClassName("close")[0];

  // When the user clicks on the button, open the modal
  $(btn).on('click', function(event){
      event.preventDefault();
      modal.style.display = "block";
    });

  // When the user clicks on <span> (x), close the modal
  // span.onclick = function() {
  //   modal.style.display = "none";
  // }

  // When the user clicks anywhere outside of the modal, close it
  window.onclick = function(event)
  {
    if (event.target == modal)
    {
      modal.style.display = "none";
    }

  }
});

function displayLogin()
{
  var login = document.getElementById("login-test");
  login.style.display = "block";
}
function toggleLoginRegister()
{
    var login = document.getElementById("login-test");
    var register = document.getElementById("register-test");
    if(login.style.display == "none")
    { //show login/hide register
        login.style.display = "block";
        register.style.display = "none";

    }
    else
    {  //show register/hide login
        login.style.display = "none";
        register.style.display = "block";
    }

}
/* When the user clicks on the button,
toggle between hiding and showing the dropdown content */
function myFunction() {
  document.getElementById("myDropdown").classList.toggle("show");
}

$(function(){
  $(document).on('click', 'button[data-id]', function (e) {
    event.preventDefault();
      var requested_to = $(this).attr('data-id')
      id = requested_to;
      var url = "/lodging/view/"+id;
      var request = "lodging/"+id;
      window.location = url;
      $.ajax({
          url: request,
          method: "GET"
      }).done(function(data) {

        if(data.status == 'error')
        {
          var errors = '<ul>';
          $.each( data.message, function( key, value )
          {
            errors = errors +'<li>'+value.msg+'</li>';
          });
          errors = errors+ '</ul>';
        }
        else {
          window.location = url;
          id = data._id;
          console.log(id);
        }
    });


  });
});
// Close the dropdown menu if the user clicks outside of it
$(function(){
    var lodgingId = window.location.href.split('/view/')[1];
    var url = "/lodging/get/"+lodgingId;
    var modal = document.getElementById('bookModal');
    $("#book").on('click', function(event){
        event.preventDefault();
        var check_in_date = $("#check_in_date").val();
        var check_out_date   = $("#check_out_date").val();
        var guests   = $("#guests").val();
        $.ajax({
          url: url,
          method: "GET"
        }).done(function(data) {
            console.log(data);
            var host = data.user;
            var price = data.price;
            var address = data.address + ', '+data.city+' '+data.state+' '+data.zipcode;
            console.log(lodgingId);
            console.log(host);
            console.log(guests);
            console.log(check_in_date);
            console.log(check_out_date);
            $.ajax({
              url :"/booking/insertBooking2",
              method: "POST",
              data: {lodgingId:lodgingId, address: address,host:host,guests:guests, check_in_date:check_in_date, check_out_date:check_out_date, price:price}

            }).done(function(data){
            });
            document.getElementById("booking").innerHTML = "Thank you for booking! Check your trips to see if it was approved!";

        });
    });
});


window.onclick = function(event) {
  if (!event.target.matches('.dropbtn')) {
    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}
