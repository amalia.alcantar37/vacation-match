function loadListings(){
  var image = document.getElementById('image');

  $.ajax({
      url: "./booking/myTrips",
      method: "GET"

  }).done(function(data) {
  console.log(data);
  if(data.length == 0)
  {
    $(document).find('.announcement').html("No upcoming trips. Book a lodging!");
    image.style.display = "block";
  }
  else {
    var tbl = '';
    tbl +='<table class="table listings" >'

      //--->create table header > start
      tbl +='<thead class = "listings">';
        tbl +='<tr>';
        tbl +='<th class = "listings">Check In Date</th>';
        tbl +='<th class = "listings">Check Out Date</th>';
        tbl +='<th class = "listings">Address</th>';
        tbl +='<th class = "listings">Guests</th>';
        tbl +='<th class = "listings">Total Price</th>';
        tbl +='<th class = "listings">Status</th>';
        tbl +='<th class = "listings">Admin Status</th>';
        tbl +='<th class = "listings">Options</th>';
        tbl +='</tr>';
      tbl +='</thead>';
      //--->create table header > end


      //--->create table body > start
      tbl +='<tbody>';
      //--->create table body rows > start
  $.each(data, function(index, val)
  {
   //you can replace with your database row id
   var row_id = val['_id'];

   var check_in = new Date(val['check_in_date']);
   var check_in_day = check_in.getDate();
   var readable_check_in = check_in.toDateString();

   var check_out = new Date(val['check_out_date']);
   var check_out_day = check_out.getDate();
   var readable_check_out = check_out.toDateString();

   //Get 1 day in milliseconds
  var one_day=1000*60*60*24;

  // Convert both dates to milliseconds
  var date1_ms =check_in.getTime();
  var date2_ms = check_out.getTime();

  // Calculate the difference in milliseconds
  var difference_ms = date2_ms - date1_ms;

  // Convert back to days and return
  var days = Math.round(difference_ms/one_day);
  var totalPrice = days*val['price']
   //loop through ajax row data
   tbl +='<tr row_id="'+row_id+'">';
     tbl +='<td class = "listings"><div class="row_data" edit_type="click" col_name="lname">'+readable_check_in+'</div></td>';
     tbl +='<td class = "listings"><div class="row_data" edit_type="click" col_name="lname">'+readable_check_out+'</div></td>';
     tbl +='<td class = "listings"><div class="row_data" edit_type="click" col_name="lname">'+val['address']+'</div></td>';
     tbl +='<td class = "listings"><div class="row_data" edit_type="click" col_name="lname">'+val['guests']+'</div></td>';


     tbl +='<td class = "listings"><div class="row_data" edit_type="click" col_name="lname">'+'$'+totalPrice+'</div></td>';
     if(val['user_status']=='approved')
     {
       tbl +='<td class = "listings"><div class="row_data" edit_type="click" col_name="status" style = "color:green;">'+val['user_status']+'</div></td>';
     }
     else {
       tbl +='<td class = "listings"><div class="row_data" edit_type="click" col_name="status" style = "color:red;">'+val['user_status']+'</div></td>';
     }
     if(val['admin_status']=='approved')
     {
       tbl +='<td class = "listings"><div class="row_data" edit_type="click" col_name="status" style = "color:green;">'+val['admin_status']+'</div></td>';
     }
     else {
       tbl +='<td class = "listings"><div class="row_data" edit_type="click" col_name="status" style = "color:red;">'+val['admin_status']+'</div></td>';
     }


     //--->edit options > start
     tbl +='<td>';

       tbl +='<span class="btn_edit" > <a href="#" class=" btn-link " row_id="'+row_id+'" > Edit</a> </span>';

       //only show this button if edit button is clicked
       tbl +='<span class="btn_save"> <a href="#" class="btn-link"  row_id="'+row_id+'"> Save</a> | </span>';
       tbl +='<span class="btn_cancel"> <a href="#" class="btn-link" row_id="'+row_id+'"> Cancel</a> | </span>';

     tbl +='</td>';
     //--->edit options > end

   tbl +='</tr>';
  });

  //--->create table body rows > end

  tbl +='</tbody>';
  //--->create table body > end

  tbl +='</table>'
  //--->create data table > end

  $(document).find('.tbl_user_data').html(tbl);

  $(document).find('.btn_save').hide();
	$(document).find('.btn_cancel').hide();


  $(document).on('click', '.btn_edit', function(event)
  	{
  		event.preventDefault();
  		var tbl_row = $(this).closest('tr');

  		var row_id = tbl_row.attr('row_id');

  		tbl_row.find('.btn_save').show();
  		tbl_row.find('.btn_cancel').show();

  		//hide edit button
  		tbl_row.find('.btn_edit').hide();

  		//make the whole row editable
  		tbl_row.find('.row_data')
  		.attr('contenteditable', 'true')
  		.attr('edit_type', 'button')
  		.addClass('bg-warning')
  		.css('padding','3px')

  		//--->add the original entry > start
  		tbl_row.find('.row_data').each(function(index, val)
  		{
  			//this will help in case user decided to click on cancel button
  			$(this).attr('original_entry', $(this).html());
  		});
  		//--->add the original entry > end

  	});
  	//--->button > edit > end


  	//--->button > cancel > start
  	$(document).on('click', '.btn_cancel', function(event)
  	{
  		event.preventDefault();

  		var tbl_row = $(this).closest('tr');

  		var row_id = tbl_row.attr('row_id');

  		//hide save and cacel buttons
  		tbl_row.find('.btn_save').hide();
  		tbl_row.find('.btn_cancel').hide();

  		//show edit button
  		tbl_row.find('.btn_edit').show();

  		//make the whole row editable
  		tbl_row.find('.row_data')
  		.attr('edit_type', 'click')
  		.removeClass('bg-warning')
  		.css('padding','')

  		tbl_row.find('.row_data').each(function(index, val)
  		{
  			$(this).html( $(this).attr('original_entry') );
  		});
  	});
  //--->save whole row entery > start
  	$(document).on('click', '.btn_save', function(event)
  	{
  		event.preventDefault();
  		var tbl_row = $(this).closest('tr');

  		var row_id = tbl_row.attr('row_id');


  		//hide save and cacel buttons
  		tbl_row.find('.btn_save').hide();
  		tbl_row.find('.btn_cancel').hide();

  		//show edit button
  		tbl_row.find('.btn_edit').show();


  		//make the whole row editable
  		tbl_row.find('.row_data')
  		.attr('edit_type', 'click')
  		.removeClass('bg-warning')
  		.css('padding','')

  		//--->get row data > start
      tbl_row.find('.row_data').each(function(index, val)
      {
        if(index!=5)
        {
          $(this).html( $(this).attr('original_entry') );
        }
        else {
          var col_val  =  $(this).html();
          if(col_val == "approved")
          {
            $.ajax({
              url:"booking/update/"+row_id,
              method:"PUT",
              data:{status:col_val}
            }).done(function(data){
              console.log("success");
            });
          }
          else if (col_val == "rejected") {
            $.ajax({
              url:"booking/update/"+row_id,
              method:"PUT",
              data:{status:col_val}
            }).done(function(data){
              console.log("success");
            });
          }
          else if (col_val == "pending") {
            $.ajax({
              url:"booking/update/"+row_id,
              method:"PUT",
              data:{status:col_val}
            }).done(function(data){
              console.log("success");
            });
          }
          else {
            $(this).html( $(this).attr('original_entry') );
          }
        }

      });

  	});
  	//--->save whole row entery > end
  }

});
}

$(function(){

    $("#request").on('click', function(event){
        event.preventDefault();
        // Get the modal
        var modal = document.getElementById('booking-modal');
        modal.style.display = "block";
        window.onclick = function(event)
        {
          if (event.target == modal)
          {
            modal.style.display = "none";
          }

        }

    });
});
window.onload = loadListings;
