const mongoose = require("mongoose");
const dbURI = "mongodb+srv://aalcanta:Seulax15@cluster0-djpuv.gcp.mongodb.net/dbvacationmatch?retryWrites=true";

const options = {
  reconnectTries: Number.MAX_VALUE,
  poolSize: 10
};

//connect to MongoDB Atlas cluster
mongoose.connect(dbURI, options).then(
  () => {
    console.log("Database connection established!");
  },
  err => {
    console.log("Error connecting Database instance due to: ", err);
  }
);
// require any models
require("./models/user");
require("./models/lodging");
