var express   = require('express');
var router    = express.Router();
var mongoose  = require('mongoose');
var multer = require('multer');
var session = require('express-session');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
//detailed way of storing files
var storage = multer.diskStorage({
  destination: function(req, file, cb){
    cb(null, 'uploads/');
  },
  filename: function(req, file, cb){
    cb(null,file.originalname);
  }
});
var fileFilter = (req, file, cb) =>{
  if(file.mimetype === 'image/jpeg' || file.mimetype === 'image/png'){
    cb(null,true);
  }
  else {
    //reject a file
    cb(null, false);
  }
};
var upload = multer({storage:storage,
  limits:{
    fileSize: 1024*1024*5
  },
  fileFilter:fileFilter
});

const ObjectId = require('mongodb').ObjectID;

var Lodging = mongoose.model('Lodging');
const { check, validationResult } = require('express-validator/check');
const { matchedData, sanitize }   = require('express-validator/filter');

//get all lodgings
router.get('/', function(req, res, next) {

  // Lodging.find({status:"pending"}).exec(function(err, lodging)
  // {
  //   if(err)
  //   {
  //     console.log('There are no requests');
  //     return next(err)
  //   }
  //   else
  //   {
  //     res.render('admin', {lodging: lodging} );
  //   }
  // });
  console.log("Fetch all lodging listings");

    User.find()
    .then(users => {
        res.send(users);
    }).catch(err => {
        res.status(500).send({
            message: err.message
        });
    });

});

//route to post new lodge
router.post('/insertLodging', [
  check('type','Type cannot be left blank')
  .isLength({ min: 1 }),
  check('address', 'Address cannot be blank')
  .isLength({ min: 1 }),
  check('city', 'City cannot be left blank')
  .isLength({ min: 1 }),
  check('zipcode','Enter a valid zipcode.')
  .isLength({ min: 1 },'Enter a valid zipcode.')
  .isNumeric()
 ],function (req,res,next){
   const errors = validationResult(req);
   if (!errors.isEmpty())
   {
     res.json({status : "error", message : errors.array()});
   }
   else
   {
     const user = req.session.user;
     const userEmail = user.email;
     const type = req.body.type;
     console.log(type);
     const address = req.body.address;
     console.log(address);
     const city = req.body.city;
     console.log(city);
     const state = req.body.state;
     console.log(state);
     const zipcode = req.body.zipcode;
     console.log(zipcode);
     const maxCap = req.body.maxCap;
     console.log(maxCap);
     const numRooms = req.body.numRooms;
     console.log(numRooms);
     const numBathrooms = req.body.numBathrooms;
     console.log(numBathrooms);
     const status = "pending";
     

     const document =
     {
      type: type,
      address : address,
      city: city,
      state: state,
      zipcode: zipcode,
      maxCap: maxCap,
      numRooms: numRooms,
      numBathrooms: numBathrooms,
      user:userEmail,
      status: status
      };
    var survey = new Lodging(document);
    survey.save(function(error){
      if(error){
        throw error;
      }
      else {
        console.log(survey._id);
        res.json({id:survey._id});
      }
    });
  }
});

//route to post pictures to database
router.post('/uploadPictures', upload.single('fileName'), function (req,res,next){

  const document = {$set: {attachments:req.file.path}};
  console.log(req.file);
  console.log(req.body.id);
  var id = new mongoose.Types.ObjectId(req.body.id);

  Lodging.findByIdAndUpdate(id, document, function(err, lodging)
  {
    if (err) return next(err);
    res.json("Lodging image uploaded!");
  });
});


//route to view lodging based on id
router.get('/view/:lodging',function(req, res,next)
{
  var id = new mongoose.Types.ObjectId(req.params.lodging);
  Lodging.findOne({_id:id}).exec(function(err, lodging)
  {
    if(err){
      console.log('No such lodging exists');
      return next(err)
    }
    else
    {
      res.render('viewLodge', {lodging:lodging} );
    }
  });
});
router.get('/myListings', function(req,res,next){
  user = req.session.user;
  email = user.email;
  Lodging.find({user:email}).exec(function(err, lodging)
  {
    if(err){
      console.log('There are no requests');
      return next(err)
    }
    else
  {
    console.log(lodging);
    res.render('listingStatus', {lodging: lodging});
  }
  });

});
router.get('/myListing', function(req,res,next){
  user = req.session.user;
  email = user.email;
  Lodging.find({user:email}).exec(function(err, lodging)
  {
    if(err){
      console.log('There are no requests');
      return next(err)
    }
    else
  {
    console.log(lodging);
    res.json(lodging);
  }
  });

});
//route to review lodging based on id
router.get('/get/:lodging',function(req, res,callback)
{
  var id = new mongoose.Types.ObjectId(req.params.lodging);
  Lodging.findOne({_id:id}).exec(function(err, lodging)
  {
    if(err){
      console.log('No such lodging exists');
      return next(err)
    }
    else
    {
      res.json(lodging);
    }
  });
});

router.post('/getLodging', function (req,res,next) {
  lodgingID = req.body.id
  o_id = new ObjectId(lodgingID);
  var id = new mongoose.Types.ObjectId(lodgingID);
  Lodging.find({_id:id}).exec(function(err, lodging)
  {
    if(err){
      console.log('There are no requests');
      return next(err)
    }
    else if(lodging == null)
    {
      console.log("fu");
    }
    else
    {
    console.log(lodging);
    console.log("---that was in lodging routter--");
    const lodge = lodging;
    res.json(lodging);
    }
  });
});

//update lodging status based on id
router.put('/update/:lodging',function(req,res)
  {
    var status = req.body.status;
    //o_id = new ObjectId(lodgingID);
    var id = new mongoose.Types.ObjectId(req.params.lodging);
    var newStatus = { $set: {status:status} };
    Lodging.findByIdAndUpdate(id, newStatus,
      function(err, lodging)
      {
        if (err) return next(err);
        res.send("Lodging status updated");
      });
  });

//update lodging description based on id
router.put('/updateDescription/:lodging',function(req,res)
    {
      var description = req.body.description;
      var price = req.body.price;
      var id = new mongoose.Types.ObjectId(req.params.lodging);
      var newParameter = { $set: {description:description, price:price} };

      Lodging.findByIdAndUpdate(id, newParameter,
        function(err, lodging)
        {
          if (err) return next(err);
          res.send("Lodging description and price added");
        });
    });
module.exports = router;
