var express   = require('express');
var router    = express.Router();
var mongoose  = require('mongoose');
var User      = mongoose.model('User');
var Lodging = mongoose.model('Lodging');
var bcrypt = require('bcrypt-nodejs');

const { check, validationResult } = require('express-validator/check');
const { matchedData, sanitize }   = require('express-validator/filter');

router.get('/', function(req, res, next) {
  Lodging.find({status:"approved"}).exec(function(err, lodging) {
    if(err) {
      console.log('There are no requests');
      return next(err);
    }
    else
    {
      res.render('index', {lodging: lodging});
    }
  });
});

  /* POST user registration page. */
  router.post('/register',[
    check('full_name','Name cannot be  left blank')
    .isLength({ min: 1 }),
    check('email', 'Email cannot be blank')
    .isEmail().withMessage('Please enter a valid email address')
    .trim()
    .normalizeEmail()
    .custom(value => {
        return findUserByEmail(value).then(User => {
          //if user email already exists throw an error
      })
    }),
    check('password')
    .isLength({ min: 5 }).withMessage('Password must be at least 5 chars long')
    .matches(/\d/).withMessage('Password must contain one number')
    .custom((value,{req, loc, path}) => {
      if (value !== req.body.cpassword) {
          // throw error if passwords do not match
          throw new Error("Passwords don't match");
      } else {
          return value;
      }
  }),
    check('dob','Date of birth cannot be left blank')
    .isLength({ min: 1 }),
   ], function(req, res, next)
   {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {

        res.json({status : "error", message : errors.array()});
    } else {

        if(req.body.password){
          password = req.body.password;
          var hashedPassword = bcrypt.hashSync(password);

        }
        var document = {
            full_name:   req.body.full_name,
            email:       req.body.email,
            password:    hashedPassword,
            dob:         req.body.dob,
            type: "user"
          };
        var user = new User(document);
        user.save(function(error){
          console.log(user);
          if(error){
            throw error;
          }
          res.json({message : "Data saved successfully.", status : "success"});
       });
    }
});

/* POST user login */
router.post('/login', function(req, res) {
  var email = req.body.email;
  var password = req.body.password;

  //check if password is entered
  if (req.body.password)
  {
    //find user with email
    User.findOne({email:email}, function(err, user)
    {
      if(err)
      {
        console.log(err);
        return res.status(500).send("5 hunna error");
      }
      //if user does not exist respond with an error
      else if (!user)
      {
         return res.json({status : "error", message : "Email not found."});
        //return res.status(404).send("No user found.");
      }
      else
      {
        let hashedPassword = user.password;
        //compare entered password with hashed password
        if (bcrypt.compareSync(password, hashedPassword))
        {
          req.session.user = user;
          console.log(req.session.user);
          res.redirect('/viewAllListings');
        }
        else
        {
          return res.json({status : "error", message : "Incorrect password."});
        }
      }
    });
  }
  else
  {
    var err = new Error('All fields required.');
    return res.status(400);
  }
});
/* POST user login */

router.get('/logout', function(req, res, next) {
  if (req.session) {
    // delete session object
    req.session.destroy(function(err) {
      if(err) {
        return next(err);
      } else {
        return res.redirect('/');
      }
    });
  }
});

function findUserByEmail(email){
  if(email){
      return new Promise((resolve, reject) => {
        User.findOne({ email: email })
          .exec((err, doc) => {
            if (err) return reject(err)
            if (doc) return reject(new Error('This email already exists. Please enter another email.'))
            else return resolve(email)
          })
      })
    }
 }


module.exports = router;
